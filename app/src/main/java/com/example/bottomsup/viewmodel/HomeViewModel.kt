package com.example.bottomsup.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.ShibeRepo
import kotlinx.coroutines.launch

class HomeViewModel: ViewModel() {
    private val repo by lazy { ShibeRepo }
    val TAG = "HomeViewModel"

    private val _state = MutableLiveData<List<String>>()
    val state : LiveData<List<String>> get() = _state

    init {
        viewModelScope.launch {
            Log.e(TAG, "init: Getting initial images ")
            val shibeDTO = repo.getShibs()
            Log.e(TAG, "init: initial Shibe's were $shibeDTO")
            _state.value = shibeDTO
        }
    }
}