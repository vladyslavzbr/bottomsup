package com.example.bottomsup.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bottomsup.databinding.ItemShibeBinding
import kotlinx.coroutines.withContext


class HomeAdapter :
    RecyclerView.Adapter<HomeAdapter.InputViewHolder>() {
    private var shibe_images = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemShibeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        val shibe_image = shibe_images[position]
        holder.loadCategory(shibe_image)
    }

    override fun getItemCount(): Int {
        return shibe_images.size
    }

    fun addCategory(category: List<String>) {
        val oldSize = shibe_images.size
        shibe_images.clear()
        notifyItemRangeChanged(0, oldSize)
        shibe_images = category.toMutableList()
        notifyItemRangeChanged(0, category.size)
    }

    class InputViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadCategory(shibe_image: String) {
            Glide.with(binding.root.context).load(shibe_image).into(binding.shibeImage)
        }
    }

}
