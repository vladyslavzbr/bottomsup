package com.example.bottomsup.model

import android.util.Log
import com.example.bottomsup.model.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ShibeRepo {
    private val shibeService: ShibeService by lazy { ShibeService.getInstance() }
    suspend fun getShibs() = withContext(Dispatchers.IO) {
        val list = shibeService.getShibs()
        Log.e("ShibeRepo", "getShibs: Shib list in reop $list")
        list
    }
}