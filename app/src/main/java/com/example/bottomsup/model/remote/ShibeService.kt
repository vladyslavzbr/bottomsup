package com.example.bottomsup.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {
    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val QUERY_CATEGORY = "count"
        private const val QUERY_ID  = "i"

        fun getInstance(): ShibeService {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(ShibeService::class.java)
        }
    }

    @GET("/api/shibes")
    suspend fun getShibs(@Query(QUERY_CATEGORY) number: Int = 100): List<String>

}