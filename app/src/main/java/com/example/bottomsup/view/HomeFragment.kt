package com.example.bottomsup.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.bottomsup.adapter.HomeAdapter
import com.example.bottomsup.databinding.FragmentHomeBinding
import com.example.bottomsup.viewmodel.HomeViewModel

class HomeFragment : Fragment() {
    val TAG = "HomeFragment"
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel by viewModels<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e(TAG, "onViewCreated: Adapter set up")
        with(binding.rv) {
            layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)
            adapter = HomeAdapter()
        }
        homeViewModel.state.observe(viewLifecycleOwner) { state ->
            Log.e(TAG, "onViewCreated: State was $state")
            (binding.rv.adapter as HomeAdapter)
                .addCategory(state)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}